<?php

use Monolog\Logger;

return[
    'database' =>[
        'name' => 'cursphp7',
        'username' => 'root',
        'password' => '1234',
        'connection' => 'mysql:host=mysql',
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true
        ]
    ],
    'logs' => [
        'filename' => 'curso.log',
        'level' => Logger::INFO,
    ],

    'routes' => [
        'filename' => 'routes.php',
    ],

    'project' => [
        'namespace' => 'cursophp7',
    ],
    'mail' => [
        'smtp_server' => 'smtp.gmail.com',
        'smtp_port' => '587',
        'smtp_security' => 'tls',
        'username' => 'ivanmorenoripoll@gmail.com',
        'password' => 'Trompa199786!',
        'email' => 'ivanmorenoripoll@gmail.com',
        'name' => 'ivanCursoPhp7',
    ],

    'security' => [
        'roles' => [
            'ROLE_ADMIN' => 3,
            'ROLE_USER' => 2,
            'ROLE_ANONYMOUS' => 1
        ]
    ]
];