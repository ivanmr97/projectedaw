<?php

use cursophp7\app\repository\AssociadosRepository;
use cursophp7\app\repository\ImagenGaleriaRepository;
use cursophp7\app\utils\Utils;
use cursophp7\core\App;


    $categorias = ['category1'=>TRUE,'category2'=>FALSE,'category3'=>FALSE];
    $imagenes =  App::getRepository(ImagenGaleriaRepository::class)->findAll();
    $asociados = App::getRepository(AssociadosRepository::class)->findAll();

    if(count($asociados) > 3) {
        $asociados = Utils::extraerTres($asociados);
    }

require_once(__DIR__.'/../views/index.view.php');
