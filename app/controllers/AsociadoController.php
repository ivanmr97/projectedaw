<?php

namespace cursophp7\app\controllers;

use cursophp7\app\entity\Associat;
use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\FileException;
use cursophp7\app\repository\AssociadosRepository;
use cursophp7\app\utils\File;
use cursophp7\app\utils\GenPDF;
use cursophp7\core\App;
use cursophp7\core\helpers\FlashMessage;
use cursophp7\core\Response;

class AsociadoController
{
    public function asociados(){
        $errores = [];
        $nombre = '';
        $descripcion = '';
        $mensaje = '';
        $email = '';
        $ascRepository = App::getRepository(AssociadosRepository::class);
        $asociados = $ascRepository->findAll();

        Response::renderView('associats', 'layout', compact('errores', 'nombre', 'descripcion', 'mensaje','email', 'asociados'));

    }

    /**
     * @throws AppException
     */
    public function nuevoAsociado(){
        try {


            $descripcion = trim(htmlspecialchars($_POST['descripcion']));
            $nombre = trim(htmlspecialchars($_POST['nombre']));
            $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
            $email = trim(htmlspecialchars($_POST['email']));
            $imagen = new File('imagen', $tiposAceptados);
            $imagen->saveUploadedFile(Associat::RUTA_IMATGES_ASSOCIATS);

            $imagenAsociado = new Associat($nombre, $imagen->getFileName(), $descripcion, $email);
            $ascRepository = App::getRepository(AssociadosRepository::class);
            $ascRepository->guarda($imagenAsociado);


        } catch (FileException $fileException) {
            $errores[] = $fileException->getMessage();
        }  catch (AppException $appException){
            $errores[] = $appException->getMessage();
        }

        App::get('router')->redirect('asociados');
    }

    public function borrarAsociado($id){

        $ascRepository = App::getRepository(AssociadosRepository::class);
        $ascRepository->eliminar($id);
        App::get('router')->redirect('asociados');
    }

    public function modificarAsociado(int $id){

        /** @var AssociadosRepository $ascRepository */
        $ascRepository = App::getRepository(AssociadosRepository::class);
        $asociado = $ascRepository->findOneBy(array('id' => $id));

        $nombre = trim(htmlspecialchars($_POST['nombre']));
        $asociado->setNombre($nombre);

        $email= trim(htmlspecialchars($_POST['email']));
        $asociado->setEmail($email);
        $descripcion = trim(htmlspecialchars($_POST['descripcion']));
        $asociado->setDescripcion($descripcion);

        $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];

        try {
            $imagen = new File('imagen', $tiposAceptados);
            $imagen->saveUploadedFile(Associat::RUTA_IMATGES_ASSOCIATS);
        } catch (FileException $e) {

        }
        if(!empty($imagen) && !is_null($imagen)) {
            $asociado->setLogo($imagen->getFileName());
        }
        $ascRepository->update($asociado);

        App::get('router')->redirect('asociados');
    }

    public function modificarAsociadoView($id)
    {
        $asociado = App::getRepository(AssociadosRepository::class)->find($id);
        $nombre = $asociado->getNombre();
        $errores = FlashMessage::get('errores', []);
        $descripcion = $asociado->getDescripcion();
        $email = $asociado->getEmail();
        Response::renderView('asociados-mod', 'layout', compact('asociado', 'nombre', 'descripcion', 'email', 'errores'));
    }

    public static function renderHTML()
    {

        $ascRepository = App::getRepository(AssociadosRepository::class);
        $asociados = $ascRepository->findAll();


        $asd = Response::renderAsociadosModView('asociadospdf', compact('asociados'));
        /** @var GenPDF $htmltoPDF */
        $htmltoPDF = App::get('htmltoPDF');
        $htmltoPDF->crearPDF($asd);
    }
}