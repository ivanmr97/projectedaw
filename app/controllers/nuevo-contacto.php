<?php

use cursophp7\core\App;

$nombre = htmlspecialchars($_POST['Nombre']);
$apellido = htmlspecialchars($_POST['Apellido']);//no obligatorio
$email = htmlspecialchars($_POST['Email']);
$subject = htmlspecialchars($_POST['Subject']);
$mensaje = htmlspecialchars($_POST['Mensaje']); //no obligatorio
$mostrarErrores = "";

if(!empty (trim($nombre))){

}else{
    $mostrarErrores = $mostrarErrores . "Debes rellenar el campo Nombre. ";
}
if(!empty (trim($email))){
    if(filter_var($email, FILTER_VALIDATE_EMAIL)){

    }else{
        $mostrarErrores = $mostrarErrores . "El correo no es valido. ";
    }
}else{
    $mostrarErrores = $mostrarErrores . "Debes rellenar el campo Email. ";
}

if (!empty (trim($subject))){

}else{
    $mostrarErrores = $mostrarErrores . "Debes rellenar el campo Subject. ";
}

App::get('router')->redirect('contact');