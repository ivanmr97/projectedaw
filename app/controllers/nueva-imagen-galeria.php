<?php

use cursophp7\app\entity\ImagenGaleria;
use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\FileException;
use cursophp7\app\exceptions\QueryException;
use cursophp7\app\repository\ImagenGaleriaRepository;
use cursophp7\app\utils\File;
use cursophp7\core\App;



try {

        $descripcion = trim(htmlspecialchars($_POST['descripcion']));
        $categoria = trim(htmlspecialchars($_POST['categoria']));
        if(empty($categoria)){
            throw new AppException("No se ha recibido la categoria");
        }
        $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
        $imagen = new File('imagen', $tiposAceptados);
        $imagen->saveUploadedFile(ImagenGaleria::RUTA_IMATGES_GALERIA);
        $imagen->copyFile(ImagenGaleria::RUTA_IMATGES_GALERIA, ImagenGaleria::RUTA_IMATGES_PORTFOLIO);

        $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);

        $imgRepository = App::getRepository(ImagenGaleriaRepository::class);
        $imgRepository->guarda($imagenGaleria);

        $message = "Se ha guardado una nueva imagen: ". $imagenGaleria->getNombre();
        App::get('logger')->add($message);


} catch (FileException $fileException) {
    die($fileException->getMessage());
} catch (QueryException $queryException) {
    die($fileException->getMessage());
}catch (AppException $appException){
    die($appException->getMessage());
}

App::get('router')->redirect('imagenes-galeria');