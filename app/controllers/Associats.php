<?php

use cursophp7\app\repository\AssociadosRepository;
use cursophp7\core\App;

$errores = [];
$nombre = '';
$descripcion = '';
$mensaje = '';

$ascRepository = App::getRepository(AssociadosRepository::class);
$asociados = $ascRepository->findAll();

require __DIR__.'/../views/associats.view.php';