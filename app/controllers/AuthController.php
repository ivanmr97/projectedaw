<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 6/11/18
 * Time: 16:59
 */

namespace cursophp7\app\controllers;

use cursophp7\app\entity\Usuario;
use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\ValidationException;
use cursophp7\app\repository\UsuarioRepository;
use cursophp7\core\App;
use cursophp7\core\helpers\FlashMessage;
use cursophp7\core\Response;

class AuthController
{
    public function login(){
        $errores = FlashMessage::get(('login-error'),[]);
        $username = FlashMessage::get('username');
        Response::renderView('login', 'layout', compact('errores', 'username'));
    }
    public function logout(){
        if (isset($_SESSION['loguedUser'])){
            $_SESSION['loguedUser'] = null;
            unset($_SESSION['loguedUser']);
        }

        App::get('router')->redirect('login');
    }

    public function register(){
        $errores = FlashMessage::get(('register-error'),[]);
        $username = FlashMessage::get('username');
        Response::renderView('register', 'layout', compact('errores', 'username'));
    }

    /**
     * @throws AppException
     */
    public function checkLogin(){
       try{
           if (!isset($_POST['username'])|| empty($_POST['username']))
               throw new ValidationException("Debes introducir el usuario y el password");
               FlashMessage::set('username', $_POST['username']);

           if (!isset($_POST['password']) || empty($_POST['password']))
               throw new ValidationException("Debes introducir el usuario y el password");



        $usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
            'username' => $_POST['username'],
            'password' => $_POST['password']
        ]);

        if(!is_null($usuario)){
            $_SESSION['loguedUser'] = $usuario->getId();
            FlashMessage::unset('username');
            App::get('router')->redirect('');
        }
        throw new ValidationException("El usuario y password introducidos no existen");
       }catch (ValidationException $exception){
           FlashMessage::set('login-error', [$exception->getMessage()]);
           App::get('router')->redirect('login');
       }
    }

    public function checkRegister(){
        try{
            if (!isset($_POST['username'])|| empty($_POST['username']))
                throw new ValidationException("Debes introducir el usuario y el password");
                FlashMessage::set('username', $_POST['username']);

            if (!isset($_POST['password']) || empty($_POST['password']))
                throw new ValidationException("Debes introducir el usuario y el password");

            $usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
                'username' => $_POST['username']
            ]);

            if(is_null($usuario)){

                $guardarUsario = new Usuario($_POST['username'], $_POST['password']);
                $usrRepository = App::getRepository(UsuarioRepository::class);
                $usrRepository->guarda($guardarUsario);

                App::get('router')->redirect('login');
            }else{
                throw new ValidationException("El usuario ya existe");
                App::get('router')->redirect('login');
            }

        }catch (ValidationException $exception){
            FlashMessage::set('register-error', [$exception->getMessage()]);
            App::get('router')->redirect('register');
        }
    }
}