<?php

namespace cursophp7\app\controllers;

use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\FileException;
use cursophp7\app\exceptions\QueryException;
use cursophp7\app\repository\CategoriaRepository;
use cursophp7\app\repository\ImagenGaleriaRepository;
use cursophp7\core\App;
use cursophp7\app\entity\ImagenGaleria;
use cursophp7\app\utils\File;
use cursophp7\core\helpers\FlashMessage;
use cursophp7\core\Response;

class ImagenGaleriaController
{

    public function galeria()
    {

        try {
            $imagenes = App::getRepository(ImagenGaleriaRepository::class)->findAll();
            $categorias = App::getRepository(CategoriaRepository::class)->findAll();
            $errores = FlashMessage::get('errores', []);
            $mensaje = FlashMessage::get('mensaje');
            $descripcion = FlashMessage::get('descripcion');
            $categoriaSeleccionada = FlashMessage::get('categoriaSeleccionada');



            Response::renderView('galeria', 'layout', compact('imagenes', 'categorias', 'errores', 'mensaje', 'descripcion', 'categoriaSeleccionada'));

        } catch (QueryException $queryException) {
            $errores[] = $queryException->getMessage();
        } catch (AppException $appException){
            $errores[] = $appException->getMessage();
        }


    }

    public function nuevaImagenGaleria(){

        try {

            $descripcion = trim(htmlspecialchars($_POST['descripcion']));
            FlashMessage::set('descripcion',$descripcion);
            $categoria = trim(htmlspecialchars($_POST['categoria']));
            if(empty($categoria)){
                throw new AppException("No se ha recibido la categoria");
            }
            FlashMessage::set('categoriaSeleccionada',$categoria );
            $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
            $imagen = new File('imagen', $tiposAceptados);
            $imagen->saveUploadedFile(ImagenGaleria::RUTA_IMATGES_GALERIA);
            $imagen->copyFile(ImagenGaleria::RUTA_IMATGES_GALERIA, ImagenGaleria::RUTA_IMATGES_PORTFOLIO);

            $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);

            $imgRepository = App::getRepository(ImagenGaleriaRepository::class);
            $imgRepository->guarda($imagenGaleria);

            $message = "Se ha guardado una nueva imagen: ". $imagenGaleria->getNombre();

            FlashMessage::set('mensaje',$message);
            FlashMessage::set('descripcion',$descripcion);
            FlashMessage::unset('descripcion');
            FlashMessage::unset('categoriaSeleccionada');
            App::get('logger')->add($message);


        } catch (FileException $fileException) {
            FlashMessage::set('errores',[ $fileException->getMessage() ]);
        } catch (AppException $appException){
            FlashMessage::set('errores',[$appException->getMessage()]);
        }

        App::get('router')->redirect('imagenes-galeria');
    }

    public function show ($id){
        $imagen = App::getRepository(ImagenGaleriaRepository::class)->find($id);

        Response::renderView('show-imagen-galeria', 'layout', compact('imagen'));
    }
}