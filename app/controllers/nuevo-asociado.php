<?php

use cursophp7\app\entity\Associat;
use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\FileException;
use cursophp7\app\repository\AssociadosRepository;
use cursophp7\app\utils\File;
use cursophp7\core\App;

try {
        $descripcion = trim(htmlspecialchars($_POST['descripcion']));
        $nombre = trim(htmlspecialchars($_POST['nombre']));
        $email = trim(htmlspecialchars($_POST['email']));
        $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
        $imagen = new File('imagen', $tiposAceptados);
        $imagen->saveUploadedFile(Associat::RUTA_IMATGES_ASSOCIATS);

        $imagenAsociado = new Associat($nombre, $imagen->getFileName(), $descripcion);
        $ascRepository = App::getRepository(AssociadosRepository::class);
        $ascRepository->guarda($imagenAsociado);


} catch (FileException $fileException) {
    $errores[] = $fileException->getMessage();
}  catch (AppException $appException){
    $errores[] = $appException->getMessage();
}

App::get('router')->redirect('asociados');