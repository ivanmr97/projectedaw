<?php

namespace cursophp7\app\controllers;

use cursophp7\app\repository\AssociadosRepository;
use cursophp7\app\repository\ImagenGaleriaRepository;
use cursophp7\app\repository\MensajesRepository;
use cursophp7\app\utils\Utils;
use cursophp7\core\App;
use cursophp7\core\Response;

class PagesController
{

    public function index(){
        $categorias = ['category1'=>TRUE,'category2'=>FALSE,'category3'=>FALSE];
        $imagenes =  App::getRepository(ImagenGaleriaRepository::class)->findAll();
        $asociados = App::getRepository(AssociadosRepository::class)->findAll();

        if(count($asociados) > 3) {
            $asociados = Utils::extraerTres($asociados, 2);
        }

        Response::renderView('index', 'layout', compact('categorias', 'imagenes', 'asociados'));

    }

    public function about(){
        $mensajes = App::getRepository(MensajesRepository::class)->findAll();
        $asociados = App::getRepository(AssociadosRepository::class)->findAll();
        if(count($mensajes) > 4) {
            $mensajes = Utils::extraerTres($mensajes, 1);
        }

        Response::renderView('about', 'layout', compact('mensajes','asociados'));

    }
    public function blog(){
        Response::renderView('blog', 'layout-with-footer');
    }

    public function post(){
        Response::renderView('single_post', 'layout-with-footer');
    }
}
