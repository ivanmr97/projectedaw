<?php

namespace cursophp7\app\controllers;

use cursophp7\app\entity\Mensaje;
use cursophp7\app\exceptions\AppException;
use cursophp7\app\repository\MensajesRepository;
use cursophp7\app\utils\MyMail;
use cursophp7\app\utils\Utils;
use cursophp7\core\App;
use cursophp7\core\Response;

class ContactoController
{
    public function contacto(){
        $nombre ="";
        $apellido = "";//no obligatorio
        $email = "";
        $subject = "";
        $mensaje =  ""; //no obligatorio
        $mostrarErrores = "";
        $msgRepository = App::getRepository(MensajesRepository::class);
        $mensajes = $msgRepository->findAll();
        Response::renderView('contact', 'layout', compact('nombre', 'apellido', 'email', 'subject', 'mensaje', 'mostrarErrores','mensajes'));
    }

    /**
     * @throws AppException
     */
    public function nuevoContacto(){
        if(!empty(htmlspecialchars($_POST['Nombre'])) && !empty(htmlspecialchars($_POST['Email'])) && !empty(htmlspecialchars($_POST['Subject']))) {
            $nombre = htmlspecialchars($_POST['Nombre']);
            $apellido = htmlspecialchars($_POST['Apellido']);//no obligatorio
            $email = htmlspecialchars($_POST['Email']);
            $subject = htmlspecialchars($_POST['Subject']);
            $mensaje = htmlspecialchars($_POST['Mensaje']); //no obligatorio

            $guardarMensaje = new Mensaje($nombre, $apellido, $subject, $email, $mensaje);
            $msjRepository = App::getRepository(MensajesRepository::class);
            $msjRepository->guarda($guardarMensaje);

            //$enviarContacto = App::get('mail');
            //$enviarContacto->send($subject, $email, $nombre . " " . $apellido, $mensaje);
        }
        App::get('router')->redirect('contact');
    }


    public function eliminar($id)
    {
        $msjRepository = App::getRepository(MensajesRepository::class);
        $msjRepository->borrar($id);
        App::get('router')->redirect('contact');
    }

    public function aprobar($id)
    {
        /** @var MensajesRepository $msjRepository */
        $msjRepository = App::getRepository(MensajesRepository::class);
        $mensaje = $msjRepository->findOneBy(array('id' => $id));
        $mensaje->setAprobado(1);
        $msjRepository->aprobar($mensaje);
        App::get('router')->redirect('contact');
    }

    public function desaprobar($id)
    {
        /** @var MensajesRepository $msjRepository */
        $msjRepository = App::getRepository(MensajesRepository::class);
        $mensaje = $msjRepository->findOneBy(array('id' => $id));
        $mensaje->setAprobado(0);
        $msjRepository->aprobar($mensaje);
        App::get('router')->redirect('contact');
    }

}