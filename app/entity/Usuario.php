<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 6/11/18
 * Time: 16:51
 */

namespace cursophp7\app\entity;


use cursophp7\core\database\IEntity;

class Usuario implements IEntity
{
    private $id;
    private $username;
    private $password;
    private $role;

    /**
     * Usuario constructor.
     * @param $username
     * @param $password
     */
    public function __construct($username="", $password="")
    {
        $this->username = $username;
        $this->password = $password;
        $this->role = 'ROLE_USER';
        $this->id = null;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Usuario
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return Usuario
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Usuario
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     * @return Usuario
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }


    public function toArray(): array
    {
        return [
       'id' => $this->getId(),
       'username' => $this->getUsername(),
       'role' => $this->getRole(),
       'password' => $this->getPassword()
        ];
    }
}