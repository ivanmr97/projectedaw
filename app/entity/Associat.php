<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 4/10/18
 * Time: 17:14
 */

namespace cursophp7\app\entity;

use cursophp7\core\database\IEntity;

class Associat implements IEntity
{
    const RUTA_IMATGES_ASSOCIATS = 'images/index/associats/';
    private $nombre, $logo, $descripcion, $id, $email;

    /**
     * Associat constructor.
     * @param $nombre
     * @param $logo
     * @param $descripcion
     */
    public function __construct($nombre="", $logo="", $descripcion="", $email= "")
    {
    $this->id = null;
    $this->nombre = $nombre;
    $this->logo = $logo;
    $this->email = $email;
    $this->descripcion = $descripcion;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     * @return Associat
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }




    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nom
     * @return Associat
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     * @return Associat
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     * @return Associat
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    public function getUrlAssociat(){
        return self::RUTA_IMATGES_ASSOCIATS. $this->logo;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'email' => $this->getEmail(),
            'descripcion' => $this->getDescripcion(),
            'logo' => $this->getLogo()
        ];
    }
}