<?php
/**
 * Created by PhpStorm.
 * User: ivanm
 * Date: 01/10/2018
 * Time: 11:26
 */

namespace cursophp7\app\entity;

use cursophp7\core\database\IEntity;

class ImagenGaleria implements IEntity
{
    const RUTA_IMATGES_PORTFOLIO = 'images/index/portfolio/';
    const RUTA_IMATGES_GALERIA = 'images/index/gallery/';
    private $id, $nombre, $descripcion, $numVisualizaciones, $numLikes, $numDownloads, $categoria;

    /**
     * ImagenGaleria constructor.
     * @param string $nombre
     * @param string $descripcion
     * @param int $categoria
     * @param int $numVisualizaciones
     * @param int $numLikes
     * @param int $numDownloads
     */

    public function __construct($nombre="", $descripcion="",$categoria = 1, $numVisualizaciones=0, $numLikes=0, $numDownloads=0)
    {
        $this->id = null;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->numVisualizaciones = $numVisualizaciones;
        $this->numLikes = $numLikes;
        $this->numDownloads = $numDownloads;
        $this->categoria = $categoria;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ImagenGaleria
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     * @return ImagenGaleria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     * @return ImagenGaleria
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }

    /**
     * @param $numVisualizaciones
     * @return $this
     */
    public function setNumVisualizaciones($numVisualizaciones)
    {
        $this->numVisualizaciones = $numVisualizaciones;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumLikes()
    {
        return $this->numLikes;
    }

    /**
     * @param mixed $numLikes
     * @return ImagenGaleria
     */
    public function setNumLikes($numLikes)
    {
        $this->numLikes = $numLikes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumDownloads()
    {
        return $this->numDownloads;
    }

    /**
     * @param $numDownloads
     * @return $this
     */
    public function setNumDownloads($numDownloads)
    {
        $this->numDownloads = $numDownloads;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategoria(): int
    {
        return $this->categoria;
    }

    /**
     * @param int $categoria
     * @return ImagenGaleria
     */
    public function setCategoria(int $categoria): ImagenGaleria
    {
        $this->categoria = $categoria;
        return $this;
    }

    public function getUrlPortfolio(){
        return self::RUTA_IMATGES_PORTFOLIO . $this->nombre;
    }

    public function getUrlGallery(){
        return self::RUTA_IMATGES_GALERIA . $this->nombre;
    }
    public function __toString()
    {
        return $this->id.$this->nombre.$this->descripcion.$this->numVisualizaciones.$this->numLikes.$this->numDownloads;
    }


    public function toArray(): array
    {
       return [
           'id' => $this->getId(),
           'nombre' => $this->getNombre(),
           'descripcion' => $this->getDescripcion(),
           'numVisualizaciones' => $this->getNumVisualizaciones(),
           'numLikes' => $this->getNumLikes(),
           'numDownloads' => $this->getNumDownloads(),
           'categoria' => $this->getCategoria()
       ];
    }
}