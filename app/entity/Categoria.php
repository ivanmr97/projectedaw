<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 11/10/18
 * Time: 16:17
 */
namespace cursophp7\app\entity;

use cursophp7\core\database\IEntity;

class Categoria implements IEntity
{

    private $id;
    private $nombre;
    private $numImagenes;

    /**
     * Categoria constructor.
     * @param $nombre
     * @param $numImagenes
     */
    public function __construct($nombre='', $numImagenes=0)
    {
        $this->id = null;
        $this->nombre = $nombre;
        $this->numImagenes = $numImagenes;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     * @return Categoria
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     * @return Categoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumImagenes()
    {
        return $this->numImagenes;
    }

    /**
     * @param mixed $numImagenes
     * @return Categoria
     */
    public function setNumImagenes($numImagenes)
    {
        $this->numImagenes = $numImagenes;
        return $this;
    }



    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'numImagenes' => $this->getNumImagenes()

        ];
    }
}