<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 8/11/18
 * Time: 15:52
 */

namespace cursophp7\app\entity;


use cursophp7\core\database\IEntity;
use DateTime;

class Mensaje implements IEntity
{
    private $nombre, $apellidos, $asunto,$email,$texto, $fecha, $id, $aprobado;

    public function __construct($nombre="", $apellidos="", $asunto="", $email="", $texto="", $aprobado = 0, string $fecha='')
    {
        $this->id = null;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->asunto= $asunto;
        $this->email = $email;
        $this->texto=$texto;
        $this->fecha= new DateTime($fecha);
        $this->fecha = $this->fecha->format('Y/m/d');
        $this->aprobado = $aprobado;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(string $nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getApellidos(): string
    {
        return $this->apellidos;
    }

    /**
     * @param string $apellidos
     */
    public function setApellidos(string $apellidos): void
    {
        $this->apellidos = $apellidos;
    }

    /**
     * @return string
     */
    public function getAsunto(): string
    {
        return $this->asunto;
    }

    /**
     * @return int
     */
    public function getAprobado(): int
    {
        return $this->aprobado;
    }

    /**
     * @param int $aprobado
     */
    public function setAprobado(int $aprobado): void
    {
        $this->aprobado = $aprobado;
    }

    /**
     * @param string $asunto
     */
    public function setAsunto(string $asunto): void
    {
        $this->asunto = $asunto;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTexto(): string
    {
        return $this->texto;
    }

    /**
     * @param string $texto
     */
    public function setTexto(string $texto): void
    {
        $this->texto = $texto;
    }

    /**
     * @return DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param DateTime $fecha
     */
    public function setFecha($fecha): void
    {
        $this->fecha = $fecha;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'apellidos' => $this->getApellidos(),
            'asunto' => $this->getAsunto(),
            'email' => $this->getEmail(),
            'texto' => $this->getTexto(),
            'fecha' => $this->getFecha(),
            'aprobado' => $this->getAprobado(),
        ];
    }
}