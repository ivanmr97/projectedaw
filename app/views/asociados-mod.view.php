    <!-- Principal Content Start -->
    <div id="associats">
        <div class="container">
            <div class="col-xs-12 col-sm-8 col-sm-push-2">
                <h1>ASOCIADOS</h1>
                <hr>
                <?php include __DIR__. '/partials/show-error.part.php' ?>
                <form class="form-horizontal" action="/asociados/modificar/<?= $asociado->getId()?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="label-control">Imagen</label>
                            <input class="form-control-file" type="file" name="imagen">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="label-control">Nombre</label>
                            <textarea class="form-control" name="nombre"><?= $nombre ?></textarea>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="label-control">Email</label>
                            <textarea class="form-control" name="email"><?= $email ?></textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="label-control">Descripción</label>
                            <textarea class="form-control" name="descripcion"><?= $descripcion ?></textarea>
                            <button class="pull-right btn btn-lg sr-button" type="submit">Modificar</button>
                        </div>
                    </div>
                </form>
                <hr class="divider">
                <div class="imagenes_galeria">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Imagen</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Email</th>
                            <th scope="col">Descripcion</th>

                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row"><?= $asociado->getId()?></th>
                                <td><img src="/<?= $asociado->getUrlAssociat()?>"
                                         alt="<?= $asociado->getDescripcion()?>"
                                         title="<?= $asociado->getDescripcion()?>"
                                         width="100"></td>
                                <td><?= $asociado->getNombre()?></td>
                                <td><?= $asociado->getEmail()?></td>
                                <td><?= $asociado->getDescripcion()?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <!-- Principal Content Start -->
