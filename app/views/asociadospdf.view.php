<div class="imagenes_galeria">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Imagen</th>
            <th scope="col">Nombre</th>
            <th scope="col">Descripcion</th>

        </tr>
        </thead>
        <tbody>
        <?php /** @var Associat $imagen */foreach (($asociados ?? []) as $imagen) : ?>

            <tr>
                <th scope="row"><?= $imagen->getId()?></th>
                <td><img src="<?= $imagen->getUrlAssociat()?>"
                         alt="<?= $imagen->getDescripcion()?>"
                         title="<?= $imagen->getDescripcion()?>"
                         width="100"></td>
                <td><?= $imagen->getNombre()?></td>
                <td><?= $imagen->getDescripcion()?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>