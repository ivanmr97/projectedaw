<nav class="navbar navbar-fixed-top navbar-default">
     <div class="container">
	   	 <div class="navbar-header">
	   	 	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
	   	 		<span class="sr-only">Toggle navigation</span>
	   	 		<span class="icon-bar"></span>
	   	 		<span class="icon-bar"></span>
	   	 		<span class="icon-bar"></span>
	   	 	</button>
            <a  class="navbar-brand page-scroll" href="#page-top">
              <span>[PHOTO]</span>
            </a>
	   	 </div>
	   	 <div class="collapse navbar-collapse navbar-right" id="menu">
	   	 	<ul class="nav navbar-nav">

	   	 		<li class="<?php echo(cursophp7\app\utils\Utils::arregloMenu("/") ? "active" : "lien")?> "><a href="/"><i class="fa fa-home sr-icons"></i> Home</a></li>
	   	 		<li class="<?php echo(cursophp7\app\utils\Utils::arregloMenu("/about") ? "active" : "lien")?>"><a href="/about#"><i class="fa fa-bookmark sr-icons"></i> About</a></li>
	   	 		<li class="<?php echo(cursophp7\app\utils\Utils::arregloMenuArray(["/blog", "/post"]) ? "active" : "lien")?>"><a href="/blog#"><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
	   	 		<li class="<?php echo(cursophp7\app\utils\Utils::arregloMenu("/contact") ? "active" : "lien")?>"><a href="/contact#"><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>

                <?php if (is_null($app['user'])) : ?>
                 <li class="<?php echo(cursophp7\app\utils\Utils::arregloMenu("/login") ? "active" : "lien")?>"><a href="/login#"><i class="fa fa-user-secret sr-icons"></i> Login</a></li>
                 <li class="<?php echo(cursophp7\app\utils\Utils::arregloMenu("/register") ? "active" : "lien")?>"><a href="/register#"><i class="fa fa-user-plus sr-icons"></i> Register</a></li>
                <?php else: ?>
                <li class="<?php echo(cursophp7\app\utils\Utils::arregloMenu("/imagenes-galeria") ? "active" : "lien")?>"><a href="/imagenes-galeria#"><i class="fa fa-image sr-icons"></i> Gallery</a></li>
                <li class="<?php echo(cursophp7\app\utils\Utils::arregloMenu("/asociados") ? "active" : "lien")?>"><a href="/asociados#"><i class="fa fa-hand-o-right sr-icons"></i> Associats</a></li>
                <li class="<?php echo(cursophp7\app\utils\Utils::arregloMenu("/logout") ? "active" : "lien")?>"><a href="/logout"><i class="fa fa-sign-out sr-icons"></i> <?= $app['user']->getUsername() ?> - Logout</a></li>
	   	 	    <?php endif; ?>
            </ul>
	   	 </div>
   	 </div>
   </nav>
