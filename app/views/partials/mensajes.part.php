<div class="row feedback text-center">
    <h3>CLIENTS FEEDBACK</h3>
    <hr>

        <?php use cursophp7\app\repository\AssociadosRepository;
        use cursophp7\core\App;

        foreach ($mensajes as $mensaje) : ?>

        <?php if($mensaje->getAprobado() === 1) : ?>
         <div class="col-xs-6 col-sm-3">
             <?php
             $asociado = App::getRepository(AssociadosRepository::class)->findOneBy(array('email' => $mensaje->getEmail()));
                if(isset($asociado)) : ?>
         <img class="img-responsive" src="<?php echo $asociado->getUrlAssociat()?>" alt="">
         <?php endif; ?>
         <h5> <?= $mensaje->getNombre()?></h5>
         <q> <?= $mensaje->getTexto() ?></q>
         </div>
        <?php endif; ?>
        <?php endforeach; ?>
</div>