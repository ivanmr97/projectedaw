<?php
/**
 * Created by PhpStorm.
 * User: ivanm
 * Date: 10/10/2018
 * Time: 22:50
 */

namespace cursophp7\app\exceptions;

class QueryException extends AppException
{
    public function __construct(string $message, int $code= 500)
    {
        parent::__construct($message, $code);
    }
}