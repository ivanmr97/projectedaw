<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 11/10/18
 * Time: 17:36
 */

namespace cursophp7\app\exceptions;

class NotFoundException extends AppException
{
    public function __construct(string $message, int $code= 404)
    {
        parent::__construct($message, $code);
    }
}