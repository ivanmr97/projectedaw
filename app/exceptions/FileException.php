<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 4/10/18
 * Time: 16:08
 */

namespace cursophp7\app\exceptions;

use Exception;

class FileException extends Exception
{

    /**
     * FileException constructor.
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}