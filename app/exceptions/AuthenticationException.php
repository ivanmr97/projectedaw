<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 6/11/18
 * Time: 20:12
 */

namespace cursophp7\app\exceptions;


class AuthenticationException extends AppException
{
    public function __construct(string $message, int $code= 403)
    {
        parent::__construct($message, $code);
    }
}