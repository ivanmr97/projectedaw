<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 8/11/18
 * Time: 20:26
 */

namespace cursophp7\app\utils;


use Dompdf\Dompdf;

class GenPDF
{


    /**
     * GenPDF constructor.
     */
    public function __construct()
    {
    }

    public function crearPDF($html){

        $mipdf = new Dompdf();
        $mipdf->loadHtml($html);
        $mipdf->render();
        $mipdf->stream();
//        $mipdf->stream('FicheroEjemplo.pdf');
    }
}