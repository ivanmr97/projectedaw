<?php

namespace cursophp7\app\utils;


use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class MyMail
{

    private $smtpServer;
    private $smtpPort;
    private $smtpSecurity;
    private $username;
    private $password;
    private $email;
    private $name;

    /**
     * MyMail constructor.
     * @param $smtpServer
     * @param $smtpPort
     * @param $smtpSecurity
     * @param $username
     * @param $password
     * @param $email
     * @param $name
     */
    public function __construct($smtpServer, $smtpPort, $smtpSecurity, $username, $password, $email, $name)
    {
        $this->smtpServer = $smtpServer;
        $this->smtpPort = $smtpPort;
        $this->smtpSecurity = $smtpSecurity;
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * @param $assumpte
     * @param $mailTo
     * @param $nameTo
     * @param $text
     */
    public function send($assumpte, $mailTo, $nameTo, $text){
        $transport = (new Swift_SmtpTransport($this->smtpServer, $this->smtpPort, $this->smtpSecurity))
            ->setUsername($this->username)
            ->setPassword($this->password);


        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Create a message
        $message = (new Swift_Message($assumpte))
            ->setFrom([$this->email => $this->name])
            ->setTo([$mailTo => $nameTo])
            ->setBody($text);

        $result = $mailer->send($message);

    }

    /**
     * @param $config
     * @return MyMail
     */
    public static function load ($config){
        $myMail = new MyMail(
            $config['smtp_server'],
            $config['smtp_port'],
            $config['smtp_security'],
            $config['username'],
            $config['password'],
            $config['email'],
            $config['name']
        );

        return $myMail;
    }
}