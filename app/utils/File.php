<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 4/10/18
 * Time: 15:50
 */

namespace cursophp7\app\utils;

use cursophp7\app\exceptions\FileException;

class File
{
    private $file = '';
    private $fileName = '';

    /**
     * File constructor.
     * @param $fileName
     * @param $fileTypes
     * @throws FileException
     */

    public function __construct($fileName, $fileTypes)
    {
        $this->file = $_FILES[$fileName];
        $this->fileName = '';

        if(!isset($this->file)){
            throw new FileException("Debes seleccionar un fichero");
        }

        if($this->file['error'] !== UPLOAD_ERR_OK){
            switch ($this->file['error']) {
                case UPLOAD_ERR_INI_SIZE;
                case UPLOAD_ERR_FORM_SIZE;
                    throw new FileException("El tamaño del fichero es demasiado grande");
                case UPLOAD_ERR_PARTIAL;
                    throw new FileException("No se ha podido subir el fichero completo");
                default:
                    throw new FileException("No se ha podido subir el fichero");

            }
        }

        if(in_array($this->file['type'] , $fileTypes) === false){
            throw new FileException("Este tipo de archivo no esta soportado");
        }
    }
    public function getFileName(){
        return $this->fileName;
    }

    /**
     * @param $destino
     * @throws FileException
     */
    public function saveUploadedFile($destino){
        if(is_uploaded_file($this->file['tmp_name'])=== false){
            throw new FileException("El fichero no se ha subido mediante un formulario");
        }
        $this->fileName = $this->file['name'];
        $ruta = $destino.$this->fileName;
        if(is_file($ruta) === true){
            $idUnico = time();
            $this->fileName = $idUnico.'_'.$this->fileName;
            $ruta = $destino.$this->fileName;
        }
        if (move_uploaded_file($this->file['tmp_name'], $ruta) === false)
            throw new FileException("No se puede mover el fichero a su destino");
    }

    /**
     * @param $rutaOrigen
     * @param $rutaDestino
     * @throws FileException
     */
    public function copyFile($rutaOrigen, $rutaDestino){
        $origen = $rutaOrigen.$this->fileName;
        $destino = $rutaDestino.$this->fileName;

        if(is_file($origen) === false){
            throw new FileException("No existe el fichero $origen que estas intentando copiar");
        }
        if(is_file($destino) === true){
            throw new FileException("El fichero $destino ya existe y no se puede sobrescribir");
        }

        if(copy($origen,$destino) === false){
            throw new FileException("No se ha podido copia el fichero $origen a $destino");
        }
    }
}