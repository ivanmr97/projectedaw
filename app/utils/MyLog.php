<?php
namespace cursophp7\app\utils;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MyLog
{
    private $log;

    private $level;
    /**
     * MyLog constructor.
     * @param $filename
     * @throws \Exception
     */

    public function __construct($filename, $level)
    {
        $this->level = $level;
        $this->log = new Logger('name');
        $this->log->pushHandler(new StreamHandler($filename), $this->level);
    }

    /**
     * @param $filename
     * @param int $level
     * @return MyLog
     * @throws \Exception
     */
    public static function load($filename, $level = Logger::INFO){
        return new MyLog($filename, $level);
    }

    /**
     * @param $message
     */
    public function add($message){
        $this->log->log($this->level, $message);
    }
}