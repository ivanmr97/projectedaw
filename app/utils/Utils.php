<?php

namespace cursophp7\app\utils;


ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL);


class Utils{

    public static function arregloMenu($menu){
        return $_SERVER['REQUEST_URI'] === $menu;
    }

    public static function arregloMenuArray($array){
        foreach ($array as $menu){
            if(self::arregloMenu($menu)){
                return true;
            }
        }
        return false;

    }

    public static function extraerTres($asociados, $type){
        if($type === 1){
            shuffle($asociados);
            return array_chunk($asociados, 4)[0];
        }else{
            shuffle($asociados);
            return array_chunk($asociados, 3)[0];
        }

    }
}
