<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 11/10/18
 * Time: 16:37
 */

namespace cursophp7\app\repository;

use cursophp7\app\entity\Categoria;
use cursophp7\core\database\QueryBuilder;

class CategoriaRepository extends QueryBuilder
{
    /**
     * CategoriaRepository constructor.
     * @param string $table
     * @param string $entityClass
     * @throws \cursophp7\app\exceptions\AppException
     */

    public function __construct($table= 'categorias', $entityClass = Categoria::class)
    {
        parent::__construct($table, $entityClass);
    }

    /**
     * @param $categoria
     * @throws \cursophp7\app\exceptions\QueryException
     */
    public function nuevaImagen($categoria){
        $categoria->setNumImagenes($categoria->getNumImagenes()+1);
        $this->update($categoria);
    }
}