<?php


namespace cursophp7\app\repository;


use cursophp7\app\entity\Mensaje;
use cursophp7\core\database\QueryBuilder;

class MensajesRepository extends QueryBuilder
{
    public function __construct($table= 'mensajes', $entityClass = Mensaje::class)
    {
        parent::__construct($table, $entityClass);
    }

    public function guarda(Mensaje $mensaje){

        $fnGuardaMensaje = function () use ($mensaje){
            $this->save($mensaje);
        };

        $this->executeTransaction($fnGuardaMensaje);
    }

    public function aprobar(Mensaje $mensaje){
        $fnModificarMensaje = function () use ($mensaje){
            $this->update($mensaje);
        };
        $this->executeTransaction($fnModificarMensaje);
    }

    public function borrar($id){
        $fnBorrarMensaje = function ()use ($id){
            $this->delete($id);
        };
        $this->executeTransaction($fnBorrarMensaje);
    }
}