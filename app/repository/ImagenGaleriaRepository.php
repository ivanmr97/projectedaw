<?php

namespace cursophp7\app\repository;

use cursophp7\app\entity\ImagenGaleria;
use cursophp7\core\database\QueryBuilder;

class ImagenGaleriaRepository extends QueryBuilder
{

    /**
     * ImagenGaleriaRepository constructor.
     * @param string $table
     * @param string $entityClass
     * @throws \cursophp7\app\exceptions\AppException
     */
    public function __construct($table= 'imagenes', $entityClass = ImagenGaleria::class)
    {
        parent::__construct($table, $entityClass);
    }

    /**
     * @param ImagenGaleria $imagenGaleria
     * @return mixed
     * @throws \cursophp7\app\exceptions\AppException
     * @throws \cursophp7\app\exceptions\NotFoundException
     * @throws \cursophp7\app\exceptions\QueryException
     */
    public function getCategoria(ImagenGaleria $imagenGaleria){
        $categoriaRepository = new CategoriaRepository();
        return $categoriaRepository->find($imagenGaleria->getCategoria());
    }

    public function guarda(ImagenGaleria $imagenGaleria){

        $fnGuardaImagen = function () use ($imagenGaleria){
            $categoria = $this->getCategoria($imagenGaleria);
            $categoriaRepository = new CategoriaRepository();
            $categoriaRepository->nuevaImagen($categoria);
            $this->save($imagenGaleria);
        };

        $this->executeTransaction($fnGuardaImagen);
    }
}