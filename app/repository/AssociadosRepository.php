<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 16/10/18
 * Time: 15:52
 */

namespace cursophp7\app\repository;

use cursophp7\app\entity\Associat;
use cursophp7\core\database\QueryBuilder;

class AssociadosRepository extends QueryBuilder
{
    /**
     * AssociadosRepository constructor.
     * @param string $table
     * @param string $entityClass
     * @throws \cursophp7\app\exceptions\AppException
     */
    public function __construct($table= 'asociados', $entityClass = Associat::class)
    {
        parent::__construct($table, $entityClass);
    }


    public function eliminar($id){
        $fnModificarImagen = function ()use ($id){
            $this->delete($id);
        };
        $this->executeTransaction($fnModificarImagen);
    }

    public function modifica(Associat $associat){
        $fnModificarImagen = function () use ($associat){
            $this->update($associat);
        };
        $this->executeTransaction($fnModificarImagen);
    }


    public function guarda(Associat $imagenAsociado){

        $fnGuardaImagen = function () use ($imagenAsociado){
            $this->save($imagenAsociado);
        };

        $this->executeTransaction($fnGuardaImagen);
    }
}