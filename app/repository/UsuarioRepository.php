<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 6/11/18
 * Time: 16:55
 */

namespace cursophp7\app\repository;


use cursophp7\app\entity\Usuario;
use cursophp7\core\database\QueryBuilder;

class UsuarioRepository extends QueryBuilder
{
    public function __construct($table = 'usuarios', $entityClass = Usuario::class)
    {
        parent::__construct($table, $entityClass);
    }

    public function guarda(Usuario $usuario){

        $fnGuardaUsuario = function () use ($usuario){
            $this->save($usuario);
        };

        $this->executeTransaction($fnGuardaUsuario);
    }
}