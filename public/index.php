<?php

use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\NotFoundException;
use cursophp7\core\App;
use cursophp7\core\Request;

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL);

try{
    require __DIR__. '/../core/bootstrap.php';

   App::get('router')->direct(Request::uri(), Request::method());

}catch (AppException $appException){
    $appException->handleError();
}
