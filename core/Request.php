<?php
/**
 * Created by PhpStorm.
 * User: vesprada
 * Date: 18/10/18
 * Time: 17:20
 */

namespace cursophp7\core;

class Request
{
    public static function uri(){
        return trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),'/');
    }

    public static function method(){
        return $_SERVER['REQUEST_METHOD'];
    }
}