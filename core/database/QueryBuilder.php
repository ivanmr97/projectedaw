<?php

namespace cursophp7\core\database;

use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\NotFoundException;
use cursophp7\app\exceptions\QueryException;
use cursophp7\core\App;
use PDO;
use PDOException;

abstract class QueryBuilder
{
    /**
     * @var mixed
     */
     private $connection;
     private $table;
     private $entityClass;
    /**
     * QueryBuilder constructor.
     * @throws AppException
     */

    public function __construct($table, $entityClass)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->entityClass = $entityClass;
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundException
     * @throws QueryException
     */
    public function find($id){
        $sql = "SELECT * from $this->table WHERE  id=$id";
        $result = $this->executeQuery($sql);
        if(empty($result)){
            throw new NotFoundException("No se ha encontrado ningun elemento con id $id");
        }
        return $result[0];
    }

    public function getFilters($filters)
    {
        if(empty($filters))
            return '';

        $strFilters = [];
        foreach ($filters as $key =>$value){
            $strFilters[] = $key . '=:'. $key;

        }
        return ' WHERE ' . implode(' and ', $strFilters);

    }
    
    public function findBy($filters)
    {
        $sql= "SELECT * from $this->table" . $this->getFilters($filters);
        return $this->executeQuery($sql, $filters);
    }

    public function findOneBy($filters)
    {
        $result = $this->findBy($filters);

        if(count($result) > 0){
            return $result[0];
        }
        return null;
    }

    /**
     * @param $sql
     * @param array $parameters
     * @return mixed
     * @throws QueryException
     */
    private function executeQuery($sql, $parameters = []):array{
        $pdoStatement = $this->connection->prepare($sql);
        if($pdoStatement->execute($parameters) === false)
            throw new QueryException("No se ha podido ejecutar la query solicitada");

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->entityClass);
    }


    /**
     * @return mixed
     * @throws QueryException
     */
    public function findAll() {
        $sql = "SELECT * from $this->table";
        $pdoStatement = $this->connection->prepare($sql);
        if($pdoStatement->execute() === false)
            throw new QueryException("No se ha podido ejecutar la query solicitada");

        return $this->executeQuery($sql);
    }

    /**
     * @param IEntity $entity
     * @throws QueryException
     */
    public function save(IEntity $entity): void
    {
        try {
            $parameters = $entity->toArray();

            $sql = sprintf('insert into %s (%s) values (%s)',
                $this->table,
                implode(',', array_keys($parameters)),
                ':' . implode(', :', array_keys($parameters))
            );
            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters);
        } catch (PDOException $exception) {
            echo $exception->getMessage();
            throw new QueryException("Error al insertar en la base de datos");
        }

    }

    public function executeTransaction(callable $fnExecuteQuerys){
        try{
            $this->connection->beginTransaction();
            $fnExecuteQuerys();
            $this->connection->commit();
        }catch (PDOException $exception){
            $this->connection->rollBack();
            throw new QueryException("No se ha podido realizar la operacion");
        }
    }

    /**
     * @param $parameters
     * @return string
     */
    public function getUpdates($parameters){
        $updates ='';

        foreach ($parameters as $key => $value){
            if($key !== 'id'){
                if($updates !== ''){
                    $updates .=', ';
                }
                $updates .= $key . '=:' . $key;
            }
        }
        return $updates;
    }

    public function update(IEntity $entity){
        try{
            $parameters = $entity->toArray();
            $sql = sprintf('UPDATE %s SET %s WHERE id= :id',
                $this->table,
                $this->getUpdates($parameters)
            );
            $statement = $this->connection->prepare($sql);
            $statement->execute($parameters);

        }catch (PDOException $exception){
            echo $exception->getMessage();
            throw new QueryException ('Error al actualizar elemento con id'. $parameters['id']);
        }


    }

    public function delete($id){
        try{

            $sql = "DELETE from $this->table WHERE id=:id";
            $statement = $this->connection->prepare($sql);
            $statement->execute(array(':id'=>$id));

        }catch (PDOException $exception){
            echo $exception->getMessage();
            throw new QueryException ('Error al eliminar elemento con id'.$id);
        }


    }
}