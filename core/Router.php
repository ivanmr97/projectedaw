<?php

namespace cursophp7\core;

use cursophp7\app\exceptions\AppException;

use cursophp7\app\exceptions\AuthenticationException;
use cursophp7\app\exceptions\NotFoundException;
use TypeError;

class Router
{

    private $routes;

    private function __construct()
    {
        $this->routes = [
            'GET' => [],
            'POST' => [],
            'PUT' => [],
            'DELETE' => []
        ];
    }

    public static function load(string $file): Router
    {
        $router = new Router();
        require $file;
        return $router;
    }

    public function get(string $uri, string $controller, $role= 'ROLE_ANONYMOUS')
    {
        $this->routes['GET'][$uri] = [
            'controller' => $controller,
            'role' => $role

        ];
    }

    public function post(string $uri, string $controller, $role= 'ROLE_ANONYMOUS')
    {
        $this->routes['POST'][$uri] = [
            'controller' => $controller,
            'role' => $role

        ];
    }

    public function put(string $uri, string $controller, $role= 'ROLE_ANONYMOUS')
    {
        $this->routes['PUT'][$uri] = [
            'controller' => $controller,
            'role' => $role

        ];
    }

    public function delete(string $uri, string $controller, $role= 'ROLE_ANONYMOUS')
    {
        $this->routes['DELETE'][$uri] = [
            'controller' => $controller,
            'role' => $role

        ];
    }

    /**
     * @param string $controller
     * @param string $action
     * @param array $parameters
     * @return bool
     * @throws AppException
     */
    private function callAction(string $controller, string $action, array $parameters): bool
    {
        try {
            $controller = App::get('config')['project']['namespace'] .
                '\\app\\controllers\\' . $controller;
            $objController = new $controller();
            if (!method_exists($objController, $action))
                throw new NotFoundException(
                    "El controlador $controller no responde al action $action");

            call_user_func_array(array($objController, $action), $parameters);
            return true;
        } catch (TypeError $typeError) {
            var_dump($typeError->getMessage());
            return false;
        }
    }

    private function getParametersRoute(string $route, array $matches)
    {
        preg_match_all('/:([^\/]+)/', $route, $parameterNames);
        $parameterNames = array_flip($parameterNames[1]);
        return array_intersect_key($matches, $parameterNames);
    }

    private function prepareRoute(string $route): string
    {
        $urlRule = preg_replace(
            '/:([^\/]+)/', '(?<\1>[^/]+)', $route);
        $urlRule = str_replace('/', '\/', $urlRule);
        return '/^' . $urlRule . '\/*$/s';
    }

    /**
     * @param $uri
     * @param $method
     * @throws AppException
     */
    public function direct($uri, $method)
    {
        foreach ($this->routes[$method] as $route => $routeData) {

            $controller = $routeData['controller'];
            $minrole = $routeData['role'];
            $urlRule = $this->prepareRoute($route);
            if (preg_match($urlRule, $uri, $matches) === 1) {
                if (Security::isUserGranted($minrole) === false) {
                    if(!is_null(App::get('appUser')))
                        throw new AuthenticationException("Acceso no autorizado");
                    else
                        $this->redirect('login');
                }
                else {

                    $parameters = $this->getParametersRoute($route, $matches);
                    list($controller, $action) = explode('@', $controller);
                    if ($this->callAction($controller, $action, $parameters) === true)

                        return;
                }


            }
        }
        throw new NotFoundException('No se ha definido una ruta para esta URI');
    }

    public function redirect($path)
    {
        header('location:/' . $path);
        exit();
    }

}