<?php
/**
 * Created by PhpStorm.
 * User: ivanm
 * Date: 11/10/2018
 * Time: 13:01
 */

namespace cursophp7\core;

use cursophp7\app\exceptions\AppException;
use cursophp7\core\database\Connection;

class App
{
    private static $container = [];

    /**
     * @param $key
     * @param $value
     */
    public static function bind($key, $value){
        static::$container[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed
     * @throws AppException
     */
    public static function get($key){
        if(! array_key_exists($key, static::$container)){
            throw new AppException("No se ha encontrado la clave $key en el contendor");
        }

        return static::$container[$key];
    }

    /**
     * @return mixed
     * @throws AppException
     */
    public static function getConnection(){
        if(! array_key_exists('connection', static::$container)){
            static::$container['connection'] = Connection::make();
        }

        return static::$container['connection'];
    }

    /**
     * @param $className
     * @return mixed
     */
    public static function getRepository($className){
        if(! array_key_exists($className, static::$container)){
            static::$container[$className] = new $className;
        }

        return static::$container[$className];
    }
}